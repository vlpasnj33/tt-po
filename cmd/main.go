package main

import (
	"fmt"
	"log"
	"net/http"
	// "os"

	// "github.com/joho/godotenv"
	"github.com/rs/cors"
	"gitlab.com/vlpasnj33/tt-po/internal/adapters/handler"
	"gitlab.com/vlpasnj33/tt-po/internal/adapters/repository"
	"gitlab.com/vlpasnj33/tt-po/internal/core/services"
)

func main() {
	// if err := godotenv.Load(); err != nil {
	// 	log.Fatalf("Failed to load .env file: %v", err)
	// }

	r := repository.NewStorageRepository()
	s := services.NewService(r)
	h := handler.NewHandler(*s)
	mux := http.NewServeMux()

	mux.HandleFunc("/result", h.GetResult)

	handler := cors.Default().Handler(mux)

	fmt.Println("Server started...")
	if err := http.ListenAndServe(":8080", handler); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
