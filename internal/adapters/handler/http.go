package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/vlpasnj33/tt-po/internal/core/services"
)

type HTTPHandler struct {
	svc services.Service
}

func NewHandler(s services.Service) *HTTPHandler {
	return &HTTPHandler{svc: s}
}

func (h *HTTPHandler) GetResult(w http.ResponseWriter, r *http.Request) {
	file, _, err := r.FormFile("file")
    if err != nil {
        http.Error(w, "Failed to read the file", http.StatusBadRequest)
        return
    }
    defer file.Close()

	data, err := h.svc.GetResult(file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
