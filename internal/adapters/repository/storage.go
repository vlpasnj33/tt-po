package repository

import (
	"fmt"
	"io"

	"gitlab.com/vlpasnj33/tt-po/internal/core/domain"
	"gitlab.com/vlpasnj33/tt-po/internal/helpers"
	"gitlab.com/vlpasnj33/tt-po/internal/utils"
)

type StorageRepository struct {
	data []int
}

func NewStorageRepository() *StorageRepository {
	return &StorageRepository{
		data: make([]int, 0),
	}
}

func (r *StorageRepository) GetResult(file io.Reader) ([]*domain.Data, error) {
	var results []*domain.Data
	var fileName string 

	helpers.DecodeBzip2Data(file, fileName + ".txt")
	
	maxNum, err := utils.MaxNumber(fileName + ".txt")
	if err != nil {
		return nil, fmt.Errorf("fail to find max number: %v", err)
	}

	minNum, err := utils.MinNumber(fileName + ".txt")
	if err != nil {
		return nil, fmt.Errorf("fail to find min number: %v", err)
	}

	medianNum, err := utils.MedianNumber(fileName + ".txt")
	if err != nil {
		return nil, fmt.Errorf("fail to find median number: %v", err)
	}

	averageNum, err := utils.AverageNumber(fileName + ".txt")
	if err != nil {
		return nil, fmt.Errorf("fail to find average number: %v", err)
	}

	increasingSeq, err := utils.IncreasingSequence(fileName + ".txt")
	if err != nil {
		return nil, fmt.Errorf("fail to find increasing numbers: %v", err)
	}

	decreasingSeq, err := utils.DecreasingSequence(fileName + ".txt")
	if err != nil {
		return nil, fmt.Errorf("fail to find decreasing numbers: %v", err)
	}

	data := &domain.Data{
		MaxNumber: maxNum,
		MinNumber: minNum,
		MedianNumber: medianNum,
		AverageNumber: averageNum,
		Increasing: increasingSeq,
		Decreasing: decreasingSeq,
	}

	results = append(results, data)
	
	helpers.DeleteFile(fileName + ".txt")

	return results, nil
}
