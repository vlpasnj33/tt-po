package helpers

import (
	"compress/bzip2"
	"io"
	"os"
)

func DecodeBzip2Data(reader io.Reader, outputFileName string) error {
	outputFile, err := os.Create(outputFileName)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	bzReader := bzip2.NewReader(reader)

	_, err = io.Copy(outputFile, bzReader)
	if err != nil {
		return err
	}

	return nil
}

func DeleteFile(fileName string) error {
    err := os.Remove(fileName)
    if err != nil {
        return err
    }
    return nil
}