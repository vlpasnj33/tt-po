package domain

type Data struct {
	MaxNumber 	  int   `json:"max_number"`
	MinNumber 	  int   `json:"min_number"`
	MedianNumber  int   `json:"median_number"`
	AverageNumber int   `json:"average_number"`
	Increasing    []int `json:"increasing_numbers"`
	Decreasing    []int `json:"decreasing_numbers"`
}
