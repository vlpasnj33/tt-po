package ports

import (
	"io"

	"gitlab.com/vlpasnj33/tt-po/internal/core/domain"
)

type DataService interface {
	GetResult(file io.Reader) ([]*domain.Data, error)
}

type DataRepository interface {
	GetResult(file io.Reader) ([]*domain.Data, error)
}
