package services

import (
	"io"

	"gitlab.com/vlpasnj33/tt-po/internal/core/domain"
	"gitlab.com/vlpasnj33/tt-po/internal/core/ports"
)

type Service struct {
	repo ports.DataRepository
}

func NewService(repo ports.DataRepository) *Service {
	return &Service{repo: repo}
}

func (s *Service) GetResult(file io.Reader) ([]*domain.Data, error) {
	return s.repo.GetResult(file)
}
