package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func MinNumber(filename string) (int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, fmt.Errorf("fail open file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var minNum int

	if scanner.Scan() {
		minNum, err = strconv.Atoi(scanner.Text())
		if err != nil {
			return 0, fmt.Errorf("convert error: %v", err)
		}
	}

	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return 0, fmt.Errorf("convert data error: %v", err)
		}

		if num < minNum {
			minNum = num
		}
	}

	if err := scanner.Err(); err != nil {
		return 0, fmt.Errorf("error scan: %v", err)
	}

	return minNum, nil
}