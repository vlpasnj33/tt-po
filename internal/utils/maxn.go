package utils

import (
	"bufio"
	"os"
	"strconv"
)

func MaxNumber(filename string) (int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	var maxNum int

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		num, err := strconv.Atoi(line)
		if err != nil {
			return 0, err
		}

		if num > maxNum {
			maxNum = num
		}
	}

	if err := scanner.Err(); err != nil {
		return 0, err
	}

	return maxNum, nil
}
