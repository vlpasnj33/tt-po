package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func DecreasingSequence(filename string) ([]int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("fail open file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var currentSequence []int
	var maxSequence []int
	var lastNum int

	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return nil, fmt.Errorf("data convert error: %v", err)
		}

		if len(currentSequence) == 0 || num < lastNum {
			currentSequence = append(currentSequence, num)
		} else {
			if len(currentSequence) > len(maxSequence) {
				maxSequence = currentSequence
			}
			currentSequence = []int{num}
		}

		lastNum = num
	}

	if len(currentSequence) > len(maxSequence) {
		maxSequence = currentSequence
	}

	return maxSequence, nil
}