package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func AverageNumber(filename string) (int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, fmt.Errorf("fail open file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var sum int
	var count int

	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return 0, fmt.Errorf("data convert error: %v", err)
		}

		sum += num
		count++
	}

	if err := scanner.Err(); err != nil {
		return 0, fmt.Errorf("convert data error: %v", err)
	}

	if count == 0 {
		return 0, fmt.Errorf("empty file")
	}

	average := sum / count

	return average, nil
}