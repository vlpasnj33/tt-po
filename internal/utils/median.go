package utils

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func MedianNumber(filename string) (int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, fmt.Errorf("fail open file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var nums []int

	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return 0, fmt.Errorf("data convert error: %v", err)
		}

		nums = append(nums, num)
	}

	if err := scanner.Err(); err != nil {
		return 0, fmt.Errorf("convert data error: %v", err)
	}

	sort.Ints(nums)

	n := len(nums)

	if n%2 == 1 {
		return nums[n/2], nil
	}
	return (nums[n/2-1] + nums[n/2]) / 2, nil
}