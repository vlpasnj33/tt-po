module gitlab.com/vlpasnj33/tt-po

go 1.21.6

require (
	github.com/joho/godotenv v1.5.1
	github.com/rs/cors v1.10.1
)
